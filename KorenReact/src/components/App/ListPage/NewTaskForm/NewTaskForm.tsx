import * as React from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { MenuItem } from "@mui/material";
import { APIDBSERVER } from "../../../AppPage/App";
interface props {
  name: string,
  rerenderList: () => Promise<void>
}
interface option {
  name: string;
  value: boolean;
}
const statusOptions = [
  { name: "כן", value: true },
  { name: "לא", value: false },
];

const PopUpForm = (props: props) => {
  const [open, setOpen] = React.useState(false);
  const [status, setStatus] = React.useState("");

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setStatus(event.target.value);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setStatus("");
  };
  const getStatusValue = () => {
    let statusValue = null ;
    statusOptions.forEach((option: option) => {
      if (option.name === status) {
        statusValue = option.value;
      }
    });
    return statusValue;
  };
  const addTask = async () => {
    handleClose()
    const taskName = (
      document.getElementById("taskNameField") as HTMLInputElement
    ).value;
    const taskStatus = getStatusValue();

    try {
      const response = await fetch(
        `${APIDBSERVER}/users/add/${props.name}/${taskStatus}/${taskName}`,
        {
          method: "POST",
        }
      );

      if (response.status === 200) {
        props.rerenderList();
        alert("מטלה נוספה בהצלחה");}
      else alert("נא למלא את כל השדות, מטלה לא נוספה");
    } catch (error) {
      alert("משהו השתבש מטלה לא נוספה");
    }
  };

  return (
    <div>
      <Button variant="outlined" onClick={handleClickOpen}>
        הוסף מטלה
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>הוסף מטלה חדשה</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            id="taskNameField"
            label="שם המטלה"
            fullWidth
            variant="standard"
          />
          <TextField
            id="statusOptions"
            select
            label="האם בוצעה?"
            type="email"
            value={status}
            onChange={handleChange}
            fullWidth
            variant="standard"
          >
            {statusOptions.map((option: option) => (
              <MenuItem key={option.name} value={option.name}>
                {option.name}
              </MenuItem>
            ))}
          </TextField>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>ביטול</Button>
          <Button onClick={addTask}>הוסף</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default PopUpForm;
