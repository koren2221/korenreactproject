import ListItem from "./ListItem/listItem";
import { useEffect, useState } from "react";
import NavBar from "./NavBar/NavBar";
import SearchBar from "./SearchBar/SearchBar";
import PopUpForm from "./NewTaskForm/NewTaskForm";
import { useSelector } from "react-redux";
import { User } from "../../../Modules/userType";
import { TodoListState } from "../../../Modules/todoListStateType";
import { Item } from "../../../Modules/itemType";
import { APIDBSERVER } from "../../AppPage/App";
import { Typography, Box } from "@mui/material";
import "./ListPage.css";
import "../../AppPage/App.css";

const ListPage = () => {
  const userIdStore = useSelector((state: TodoListState) => state._id.$oid);
  const [user, setUser] = useState<User>({
    _id: { $oid: "" },
    name: "",
    todoList: [],
  });

  const getConnectedUser = async () => {
    try {
      const respose = await fetch(`${APIDBSERVER}/users/id/${userIdStore}`);
      const connectedUser = await respose.json();
      setUser(connectedUser);
    } catch {
      alert("something went wrong");
    }
  };

  useEffect(() => {
    getConnectedUser();
  }, [userIdStore]);

  const filterTasks = (todoList: Item[], query: string) => {
    if (!query) {
      return todoList;
    }

    return todoList.filter((task) => {
      const taskName = task.name.toLowerCase();
      return taskName.includes(query);
    });
  };

  const handleStatusChange = (taskId: string) => {
    if (user.todoList) {
      const tempUser = user;
      tempUser.todoList = user.todoList.map((task) => {
        if (task._id === taskId) {
          task.status = !task.status;
        }
        return task;
      });

      setUser(tempUser);
    }
  };

  const handleDelete = (taskId: string) => {
    const tempUser = { ...user };
    if (user.todoList) {
      tempUser.todoList = user.todoList.filter((task) => task._id !== taskId);
    }
    setUser(tempUser);
  };

  const [searchQuery, setSearchQuery] = useState("");
  let filteredTasks: Item[] = [];
  if (user.todoList) {
    filteredTasks = filterTasks(user.todoList, searchQuery);
  }

  return (
    <Box id="fullPage">
      <NavBar name={user.name}></NavBar>
      <Box id="list">
        <Typography id="welcomeTitle">
          {" "}
          ברוך הבא לרשימה של {user.name}
        </Typography>
        <Box id="listActionBox">
          <SearchBar
            searchQuery={searchQuery}
            setSearchQuery={setSearchQuery}
          ></SearchBar>
          <PopUpForm name={user.name} rerenderList={getConnectedUser}/>
        </Box>
        <Box id="listBox">
          {filteredTasks.map((listItem: Item) => {
            return (
              <ListItem
                listItem={listItem}
                user={user}
                handleStatusChange={handleStatusChange}
                handleDelete={handleDelete}
              />
            );
          })}
        </Box>
      </Box>
    </Box>
  );
};

export default ListPage;
