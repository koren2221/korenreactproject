import SearchIcon from "@mui/icons-material/Search";
import { Input, FormControl } from "@mui/material";
import "./SeacrhBar.css";
interface props {
  searchQuery: string;
  setSearchQuery: React.Dispatch<React.SetStateAction<string>>;
}
const SearchBar = (props: props) => {

  return (
    <FormControl id="searchBar">
      <Input
        value={props.searchQuery}
        onInput={(e) => {
          return props.setSearchQuery((e.target as HTMLInputElement).value);
        }}
        type="text"
        id="headerSearch"
        placeholder="חפש מטלה"
      />
      <SearchIcon id="icon" />
    </FormControl>
  );
};

export default SearchBar;
