import { Card, Checkbox } from "@mui/material";
import FormControlLabel from "@mui/material/FormControlLabel";
import React, { useState } from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import IconButton from "@mui/material/IconButton";
//@ts-ignore
import confetti from "https://cdn.skypack.dev/canvas-confetti";
import { Item } from "../../../../Modules/itemType";
import { User } from "../../../../Modules/userType";
import "./ListItem.css";
const APIDBSERVER = "http://localhost:4567";
interface props {
  listItem: Item;
  user: User;
  handleStatusChange: (taskId: string) => void;
  handleDelete: (taskId: string) => void;
}
const ListItem = (props: props) => {
  const [checked, setChecked] = useState(props.listItem.status);

  const deleteItem = (name: string, taskId: string) => {
    fetch(`${APIDBSERVER}/remove/${name}/${taskId}`, {
      method: "POST",
    });
    props.handleDelete(taskId);
  };

  const HandleChange = async (taskId: string, user: User) => {
    setChecked(!props.listItem.status);

    if (!checked) {
      confetti({
        spread: 100,
      });
    }

    fetch(`${APIDBSERVER}/users/${user.name}/${taskId}`, {
      method: "POST",
    });
    props.handleStatusChange(taskId);
  };

  return (
    <Card id="listCard">
      <FormControlLabel
        id="innerCard"
        control={
          <Checkbox
            checked={props.listItem.status}
            onChange={() => {
              HandleChange(props.listItem._id, props.user);
            }}
          />
        }
        label={props.listItem.name}
      />
      <IconButton
        id="deleteBtn"
        aria-label="delete"
        onClick={(event: React.MouseEvent<HTMLElement>) => {
          deleteItem(props.user.name, props.listItem._id);
        }}
      >
        <DeleteIcon />
      </IconButton>
    </Card>
  );
};

export default ListItem;
