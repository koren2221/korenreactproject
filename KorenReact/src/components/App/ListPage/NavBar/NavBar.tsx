import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import Home from "@mui/icons-material/Home";
import { useHistory } from "react-router";
import { useDispatch } from "react-redux";
import { logout } from "../../../../redux/userRedux/todoListActions";
import "./NavBar.css";
interface props{
  name: string
}
const NavBar = (props: props) => {
  const history = useHistory();
  const dispatch = useDispatch();

  const pushToRoute = (route: string) => {
    history.push(route);
  };

  return (
    <Box>
      <AppBar id="appBar">
        <Toolbar id="navBar">
          <Typography>שלום {props.name}</Typography>

          <IconButton
            id="homeIcon"
            size="large"
            onClick={() => {
              pushToRoute(`/todoList/`);
              dispatch(logout());
            }}
          >
            <Home />
          </IconButton>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default NavBar;
