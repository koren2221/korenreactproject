import {
  FormControl,
  MenuItem,
  Select,
  InputLabel,
  Card,
  Typography,
  Box,
} from "@mui/material";
import { useHistory, withRouter } from "react-router-dom";
import { useDispatch } from "react-redux";
import { ReactNode, useEffect, useState } from "react";
import { login } from "../../../../redux/userRedux/todoListActions";
import { APIDBSERVER } from "../../../AppPage/App";
import { User } from "../../../../Modules/userType";
import "./login.css";

const Login = () => {
  const dispatch = useDispatch();
  const [users, setUsers] = useState<User[]>([]);
  let history = useHistory();
  const getNames = async () => {
    const response = await fetch(`${APIDBSERVER}/users/names`);
    const result = await response.json();
    setUsers(result);
  };

  useEffect(() => {
    getNames();
  }, []);

  const pushToRoute = (route: string) => {
    history.push(route);
  };

  const usersNamesMap = (): ReactNode => {
    return users.map((user: User) => {
      return (
        <MenuItem
          onClick={() => {
            dispatch(login(user._id));
            return pushToRoute(`/todoList/listPage`);
          }}
        >
          {user.name}
        </MenuItem>
      );
    });
  };

  return (
    <Box className="center">
      <Card id="loginCard">
        <Typography id="cardTitle"> למי יש מטלות?</Typography>
        <FormControl id="dropdown">
          <InputLabel id="chooseInputLable"> בחרו..</InputLabel>
          <Select id="chooseSelect">{usersNamesMap()}</Select>
        </FormControl>
      </Card>
    </Box>
  );
};

export default Login;
