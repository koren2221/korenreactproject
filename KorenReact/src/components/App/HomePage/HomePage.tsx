import Login from "./Login/login";
import './HomePage.css';
import '../../AppPage/App.css';
import { Typography, Box } from "@mui/material";

const HomePage = () => {
    return (
        <Box id="fullPage">
            <Typography className="center" id="todoTitle">טו דו</Typography>
            <Typography className="center" id="subTitle">מה אני צריך לעשות</Typography>
            <Login />
        </Box>)
}

export default HomePage;