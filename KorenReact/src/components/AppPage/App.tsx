import { BrowserRouter, Route, Switch } from "react-router-dom";
import ListPage from "../App/ListPage/listPage";
import { Provider } from "react-redux";
import store from "../../redux/userRedux";
import HomePage from "../App/HomePage/HomePage";
import { ComponentType } from "react";
export const APIDBSERVER = "http://localhost:4567";

interface pathListItem {
  path: string,
  component: ComponentType<any>
}

const pathList = [
  { path: "/todoList/listPage", component: ListPage  },
  { path: "/todoList/", component: HomePage },
  { path: "/", component: HomePage  },
];

const App = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          {pathList.map((pathListItem: pathListItem) => {
            return(<Route path={pathListItem.path} component={pathListItem.component}/>);
          })}
          {/* <Route path="/todoList/listPage" component={ListPage} />
          <Route path="/todoList/" component={HomePage} />
          <Route path="/" component={HomePage} /> */}
        </Switch>
      </BrowserRouter>
    </Provider>
  );
};

export default App;
