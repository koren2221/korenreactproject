import { ActionType } from "typesafe-actions";
import * as actions from "./todoListActions";
import { TodoListState } from "../../Modules/todoListStateType";
import { storeActions } from "./todoListActions";
export type TodoListActions = ActionType<typeof actions>;

const initialState: TodoListState = {
  _id: { $oid: "" },
};

export const todoListReducer = (
  state: TodoListState = initialState,
  action: TodoListActions
): TodoListState => {
  switch (action.type) {
    case storeActions.login: {
      return { ...state, _id: action.payload };
    }
    case storeActions.logout: {
      return { ...state, _id: action.payload };
    }
    default:
      return state;
  }
};
