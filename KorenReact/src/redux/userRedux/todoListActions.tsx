import { action } from "typesafe-actions";
import { _id } from "../../Modules/idType";

export const storeActions = { login: "LOGIN", logout: "LOGOUT" };

export const login = (_id: _id) => {
  return action(storeActions.login, _id);
};

export const logout = () => {
  return action(storeActions.logout, {$oid:""});
};
