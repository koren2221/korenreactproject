import { createStore } from "redux";
import { todoListReducer } from "./todoListReducers";

const store = createStore(todoListReducer);

export default store;
