import App from "./components/AppPage/App";
import ReactDOM from "react-dom";

const Index = () => {
  return (
    <App />
  );
};

export default Index;
ReactDOM.render(<Index />, document.getElementById("root"));