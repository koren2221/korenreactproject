import { _id } from './idType';
import { Item } from './itemType';

export interface User {
    _id: _id,
    name: string,
    todoList?: Item[];
}


