package Modules;

public class Items {
    public String name;
    public boolean status;
    public String _id;

    public Items(String name, boolean status, String _id) {
        this.name = name;
        this.status = status;
        this._id = _id;
    }
}
