package Modules;

import org.bson.types.ObjectId;

public class Users {
    public ObjectId _id;
    public String name;
    public Items[] todoList;

    public Users(ObjectId _id, String name, Items[] todoList) {
        this._id = _id;
        this.name = name;
        this.todoList = todoList;
    }
}
