package RequestHandler;

import BusinessLayer.UserBL;
import Modules.Items;
import Modules.Users;
import com.google.gson.Gson;
import spark.Spark;

import static spark.Spark.after;
import static spark.Spark.before;

public class UserRH {
    public static void activate() {
        UserBL userBL = new UserBL();

        before("/*", (request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "*");
            response.header("Access-Control-Allow-Headers", "*");
        });

        Spark.options("/users/:name/:taskId", (((request, response) -> "okay")));

        Spark.post("/users/:name/:taskId", ((request, response) -> {
            userBL.changeTaskStatus(request.params(":name"), request.params(":taskId"));
            return "status changed";
        }));

        Spark.post("/remove/:name/:taskId", ((request, response) -> {
            userBL.removeById(request.params(":name"), request.params(":taskId"));
            return "task was deleted";
        }));

        Spark.options("/remove/:name/:taskId", (((request, response) -> "okay")));

        Spark.get("/users/names", ((request, response) -> userBL.getUsersNames()));

        Spark.get("/users/id/:id", ((request, response) -> userBL.getUserById(request.params(":id"))));

        Spark.options("/users/add/:name/:status/:taskName", (((request, response) -> "okay")));

        Spark.post("/users/add/:name/:status/:taskName", ((request, response) -> {
            userBL.AddNewTask(request.params("name"), Boolean.valueOf(request.params("status")), request.params("taskName"));
            return "task added";
        }));
    }
}
