package DataLayer;
import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

public class UserDL {
    private MongoClient mongoClient = new MongoClient();
    private DB originDB = mongoClient.getDB("KorenDB");
    private DBCollection usersCollection = originDB.getCollection("users");
    private MongoDatabase originDBs = mongoClient.getDatabase("KorenDB");
    private MongoCollection<Document> usersCollections = originDBs.getCollection("users");

    public void changeStatus(String name, String taskId){
        List<Document> users = usersCollections.find()
                .into(new ArrayList<Document>());
        for(Document doc : users)
        {
            if (doc.getString("name").equals(name)){

                ArrayList<Document> tasks = (ArrayList<Document>) doc.get("todoList");
                boolean taskStatus = true;
                for(Document embeddedDoc : tasks)
                {
                    if(embeddedDoc.getString("_id").equals(taskId))
                    {
                        taskStatus = embeddedDoc.getBoolean("status");
                    }
                }
                Bson filter  = Filters.and(
                        Filters.eq("name", name),
                        Filters.eq("todoList._id", taskId)
                );
                Bson update = Updates.combine(
                                Updates.set("todoList.$.status", !taskStatus)
                );

                usersCollections.updateOne(filter, update);
            }}
    }

    public void deleteById(String name, String taskId) {
        List<Document> users = usersCollections.find()
                .into(new ArrayList<Document>());
        for(Document doc : users)
        {
            if (doc.getString("name").equals(name)){

            ArrayList<Document> tasks = (ArrayList<Document>) doc.get("todoList");
            boolean taskStatus = true;
            String taskName = "";
            for(Document embeddedDoc : tasks)
            {
                if(embeddedDoc.getString("_id").equals(taskId))
                {
                    taskStatus = embeddedDoc.getBoolean("status");
                    taskName = embeddedDoc.getString("name");
                }
            }
            Bson studentFilter = Filters.eq( "_id", doc.get("_id") );
            Bson delete = Updates.pull("todoList", new Document("name", taskName).append("status", taskStatus).append("_id", taskId));

            usersCollections.updateOne(studentFilter, delete);
        }}
    }

    public ArrayList<DBObject> getUsersNames() {
        BasicDBObject fields = new BasicDBObject();
        fields.put("name", 1);
        BasicDBObject query = new BasicDBObject();

        DBCursor cursor =  usersCollection.find(query, fields);
        ArrayList<DBObject> users = new ArrayList<>();

        try {
            while (cursor.hasNext()){
                users.add(cursor.next());
            }
        } finally {
            cursor.close();
        }

        return users;
    }

    public DBObject getUserById(String id){
        BasicDBObject query = new BasicDBObject();
        query.append("_id", new ObjectId(id));
        return usersCollection.findOne(query);
    }

    public void addNewTask(String name, Boolean taskStatus, String taskName){
        List<Document> users = usersCollections.find()
                .into(new ArrayList<Document>());
        for(Document doc : users)
        {
            if (doc.getString("name").equals(name)){
                ArrayList<Document> tasks = (ArrayList<Document>) doc.get("todoList");
                String taskId = String.valueOf(tasks.size()+1);
                Bson studentFilter = Filters.eq( "_id", doc.get("_id") );
                Bson add = Updates.push("todoList", new Document("name", taskName).append("status", taskStatus).append("_id", taskId));

                usersCollections.updateOne(studentFilter, add);
            }}
    }
}
