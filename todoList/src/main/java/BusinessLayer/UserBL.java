package BusinessLayer;

import DataLayer.UserDL;
import com.mongodb.DBObject;

import java.util.ArrayList;

public class UserBL {
    private UserDL userDL = new UserDL();

    public void changeTaskStatus(String name, String taskId){
        userDL.changeStatus(name, taskId);
    }

    public void removeById(String name, String taskId) {
        userDL.deleteById(name, taskId);
    }

    public ArrayList<DBObject> getUsersNames(){
        return userDL.getUsersNames();
    }

    public DBObject getUserById(String id){
        return userDL.getUserById(id);
    }

    public void AddNewTask(String name, Boolean taskStatus, String taskName){
        userDL.addNewTask(name,taskStatus,taskName);
    }
}
